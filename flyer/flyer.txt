School title
Behavioural approaches for API-economy with applications


Dates & Venue
  8-12 July 2019 
  College Court Conference Centre, Knighton Road
  LE2 3UF - Leicester (UK),
  https://collegecourt.co.uk


Financial support

  Subject to budget financial support may be available. To apply email
  Emilio Tuosto (emilio@le.ac.uk) a short text detailing the amount
  and the justification for requiring financial support


Contact
  Emilio Tuosto (emilio@le.ac.uk)
  Department of Informatics
  University of Leicester


Course topics

  The school will feature theoretical and practical sessions on the
  concept of behavioural APIs. We are proud of offering a nice mix of
  courses and boot-camps from academia and industry. Many courses will
  be supported by practical hands-on sessions with state-of-the-art
  tools and technology.


Courses

- Gul Agha (University of Illinois at Urbana-Champaign, USA)
  Title: TBA
  
- Diego Garbervetsky (University of Buenos Aires, Argentina)
  Title: Using behavioral abstractions for testing and validating classes.

- Ornela Dharda (University of Glasgow, UK)
  Title: Introduction to Session Types

- Bootcamp
  Leonardo Frittelli, Facundo Maldonado, Andres More, Damian Quiroga (McAfee Cordoba, AR)
  Title: Cybersecurity in the API economy: STIX and OpenDXL 

- Bootcamp
  Roland Kuhn (Actix, DL)
  Title: Industry use-case: uncompromisingly available agents
  
- Ivan Lanese (University of Bolgna, IT)
  Title: Choreographic Programming of Adaptive Applications

- Karoliina Lihtinen (University of Liverpool. UK)
  Title: Foundations of runtime verification

- Rumyana Neykova (Middlesex University, UK)
  Title: Session Types meet Type Providers: Compile-time Generation of Protocol APIs 

- Bootcamp
  Hugo Lopez (DCR, DK) and Thomas Hildebrandt (IT University of Copenhagen, DK)
  Title: Business Process Regulatory Compliance

- Bootcamp
  Francesco Ferrari (BitLand, Bologna, IT)
  Title: TBA
  
  The school will also host a bootcamp where companies will
  showcase their approaches to API development with hands-on sessions.

 

