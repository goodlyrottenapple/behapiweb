# https://www.jasondavies.com/wordcloud/
# rectangular
# 5
# -28
# 71
# 40
# log n

behavioural approaches for API-economy with applications @ college court conference centre

the school will feature theoretical and practical sessions on the concept of behavioural API. we are proud of offering a nice mix of BehAPI and boot-camps from academia and industry.  BehAPI will be supported by practical hands on sessions with state-of-the-art tools and technology.
temporal coordination of actors: specification, inference and enforcement of mechanisms

gul agha
 familiarity with concurrent and object-oriented programming.
: in sequential systems, programmers are responsible for totally ing events occurring in a system. this results in overly constraining when events  occur. in concurrent systems, actions at autonomous actors are nondeterministically interleaved. without additional constraints on the  of events at participating actors, an interleaving  lead to incorrect operation or deadlocks. the presentation will describe several mechanisms for temporal coordination of actors: synchronization constraints, session types, specification diagrams and protocol description languages. i will survey methods for specification, inference and enforcement of these mechanisms, their applicability and limitations. the talk will conclude with a perspective on open problems and research directions.
BehAPI to session types

 ornela dardha
: in complex distributed systems, communicating participants agree on a protocol to follow, by specifying the type and direction of data exchanged. session types are an elegant type formalism  to model structured communication-based programming. they guarantee privacy, communication safety and session fidelity. a binary session type describes communication among two participants, as opposed to multiparty session type, which describes communication among two or more participants.
in these lectures we will  mainly on binary session types and the pi-calculus, a core communication and concurrency calculus, which is the original and most successful framework for session types. we will give the statics and dynamics of the language and illustrate these with several examples and exercises. we will  some advanced topics on session types as well as their logical foundations. , we will  how session types are integrated in mainstream programming languages, with main  on java, and a summary of session-based software tools for specifying and verifying communicating protocols in distributed scenarios.
 behavioral ions for testing and validating classes.

diego garbervetsky
:  software engineering artefacts, such as source code or specifications, define a set of operations and impose restrictions to the ing on which they have to be invoked. enabledness preserving ions (epas) are concise representations of the behaviour space for such artefacts. in this tutorial, we exemplify how epas might be  for validation of software engineering artefacts by showing the  of epas to support some programming tasks on a simple java/c# classes. we will also show how epas can be  for better test BehAPI generation for classes with interesting protocols.
choreographic programming of adaptive applications

 ivan lanese
some previous knowledge about choreographies or multiparty session is encouraged, but not necessary
: choreographic programming exploits choreographies as a programming language. we will introduce choreographic programming, and showBehAPI its for programming distributed applications which are:

    dynamically updatable by including BehAPI code fragments at runtime
    free by construction from errors such as communication deadlocks
    races

we will be  the following software: aiocj and jolie 
foundations of runtime verification

 karoliina lihtinen
: this BehAPI is an BehAPI to the theoretical foundations of runtime verification.
the idea of runtime verification is to observe the executions of a system in  to derive information on its behaviour. by analysing executions rather than the system itself, it avoids the state-explosion problem associated with exhaustive verification techniques such as model-checking.
the cost of lower complexity is limited expressivity: while model-checking will always BehAPI a definite answer to the question “does the system satisfy the specification”, runtime verification BehAPI no such firm guarantees. however, depending on the specification, runtime verification can BehAPI a range of weaker guarantees. understanding the trade-offs between the complexity of specifications and the power of runtime verification is at the heart of this BehAPI.
the first part of this BehAPI will be an BehAPI to runtime monitors and how to formalise their behaviour. the BehAPI will then cover different notions of correctness for runtime monitors, and  the trade-off implied by these different definitions. we will  the automatic synthesis of monitors  from specifications. the final part of the BehAPI will cover how moving from linear-time specifications to branching-time specifications affects monitorability.
the of the BehAPI is to familiarise the participants with recent efforts to give rigorous foundations to runtime verification. at the end of the BehAPI the participants will be able identify and state precisely how useful runtime verification might be for a  specification or class of specifications, and generate monitors with the appropriate correctness guarantees.
session types meet type BehAPIrs: compile-time generation of protocol API

 rumyana neykova (brunel university london)
we will BehAPI vms with the environment setup. if attendees want to install the tools in their own environment, then they will need: .net 4.5 (at least) with f#, visual studio and java8.  
: session types is a typing discipline for concurrent and distributed processes that allows errors such as communication mismatches and deadlocks to be detected statically. refinement types are types elaborated by logical constraints that allow richer and finer-grained specification of application properties, combining types with logical formulae that  refer to program values and can constrain types  arbitrary predicates. type BehAPIrs, developed in f#, are compile-time compnts for on-demand code generation. their architecture relies on an open-compiler, where BehAPIr-authors implement a small interface that allows them to inject BehAPI names/types into the programming context as the program is written.
in these lecture series, i will  a library that integrates aspects from the above fields to realise practical applications of multiparty refinement session types (mpst) for any .net language. our library supports the specification and validation of distributed message passing protocols enriched with message-type refinements (value constraints) and value dependent control flow. the protocols are written in scribble (a toolchain for mpst).
the combination of these aspects—session types for structured interactions, constraint solving from refinement types, and protocol-specific code generation enables the specification and implementation of enriched protocols in native f# (and any .net-compiled language) without requiring language extensions or external pre-processing of user programs. the API generation happens on demand (while the developer is writing the program) and at compile-time (the generated API calls are  integrated into the compiled binaries of the program).
we will  the formal foundations of the library, validation of scribble protocols, the technical challenges of compile-time generation, and , we will practice type-driven development of several small- and large-scale protocols. we will conclude with a brief overview of other tools and techniques for API generation of scribble protocols.
the impact of asynchronous communication on the theory of contracts and session types

 gianluigi zavattaro
: we  some of the proposals in the literature about contract theories and session  types dealing with asynchronous communication. assuming that communication is mediated by buffers, notions of compatibility, contract refinement and session subtyping can be defined that are less restrictive w.r.t. their synchronous counterparts. unfortunately,  most of such notions in the literature turn out to be undecidable. we  the source of this negative result, and  some proposals to overcome this limitation, with the of obtaining algorithmic versions of such notions.
bootcamp: tba

bootcamp: cybersecurity for the API economy

 leonardo frittelli, facundo maldonado, andres more, damian quiroga (mcafee)
internet connectivity;  BehAPI to stix would help, but it is not necessary.
: effectively exchanging and acting upon threat intelligence in a diverse, heterogeneous landscape such as cyber security has proven an elusive goal. with the continuous evolution of both the security tools and the attack techniques, combined with a market where BehAPI cloud based players surface on a rapid scale, having a common language to both define and apply the knowledge obtained from actual attacks is key to the success of the whole industry.

this bootcamp will  a stix 2.0 and opendxl.

stix 2.0 is a proposal the standardisation of data modelling for security products as well as application of the model through the definition of suitable patterns. after overviewing the history stix 2.0, its applications in industry, and its model, we will  on the libraries and the patterning methods. students will real world examples of threat intelligence reports to practice first hand both the model and patterning API with bespoke exercises.

opendxl is an open API to enable devices to share intelligence and orchestrate security operations in real-time. this security connected platform from mcafee BehAPI a unified framework for hundreds of products, services, and partners already adopted in the field. any organization can improve its security posture and minimize operational costs through the platform’s capabilities. the platform leverages a real-time data exchange framework to build collective threat intelligence to make endpoint, network, and cloud countermeasures protect and detect as .
the tutorial includes a review of the problem-space and BehAPI to be supported, then matching against the proposed platform capabilities including open API. after reviewing concrete examples of real-world integrations by partners and customers, we will  how to develop by leveraging current API and creating BehAPI s. to complete the tutorial student will be challenged with hands-on exercises re both python and nodered samples from opendxl to add security-related behaviors to the platform.
industry use-BehAPI: uncompromisingly available agents

roland kuhn (actyx)
in the manufacturing industry downtime is very expensive, therefore most small and midsize factories are still managed  paper-based processes. the problem space is perfectly suited for a microservices BehAPI: well-defined and locally encapsulated responsibilities, collaboration and loose coupling, rapid evolution of individual pieces. but how can we operate microservices such that they can deliver the resilience of paper? how can we leverage the locality of process data and benefit from high bandwidth and low latency communication in the internet of things?
this workshop explores the radical BehAPI of deploying autonomous actor-like agents in a peer-to-peer network on the factory shop-floor,  event sourcing as the only means of communication and observation. we  the BehAPI of a coordination-free totally ed eventually consistent event log and its consequences on the programming model inspired by the time warp algorithm (jefferson 1987). in BehAPI we dive into the question of a formal description of legal event sequences to simplify the programmer’s life: it is not yet clear what is attainable in this regard.

 
bootcamp: business process regulatory compliance

 hugo andrés lopez
internet connection and a browser. ideally, students are encouraged to create a user at this website  and install the highlighter tool. details of installation are in this youtube video.
: regulatory compliance describes the level of alignment between business processes and legislations, and it represents a complex process where regulations have to be instantiated in terms of a business process, and  check for correctness: the business process should BehAPI evidence that actors fulfilling necessary conditions will eventually be granted with the rights they are entitled to. this is important for governmental BehAPI work, that are tightly governed by law. legislations are far from being digitalised, and to BehAPI a better support for ensuring regulatory compliance of BehAPI management processes we need formalisations of the law that can be mechanised and  for determining the legal and the required activities to be carried out at a BehAPI point in a  process. this calls for a logic that can distinguish between what is possible, i.e. legal and  happen, and what is required, i.e. legal and must happen. which legal constraints are relevant at a BehAPI time for a BehAPI process often varies a lot depending on the BehAPI. this calls for a logic in which rules can be dynamically included and excluded, and where adaptation of regulations can be made in an effortless way.
moreover, BehAPI management processes often contain activities and rules that are not described in detail by the law, but still relevant for the effective support of the process. consequently, we need tools and methods for the formalisation of regulations and processes by different kinds of users and from two different kind of sources: legal texts formalised by legal experts, and descriptions of practice formalised by process experts.
in this tutorial we will  how the declarative dynamic condition response (dcr) graphs process notation and associated tools have been developed for the formalisation and mechanisation of adaptive BehAPI management processes and meets exactly these requirements. essentially, dcr graphs is a. a (declarative) process language that defines the causal relations between activities, with emphasis in what  happen (conditions) vs. what must be done (responses). it is dynamic, as these relations might change depending on the execution of certain events. b. it is a graphical language that users with no background in formal methods can use, and c. it is a business process execution engine. we will  how dcr graphs have been  in the formalisation of legally-compliant business processes in danish municipalities. in BehAPI, we will demonstrate how the recently developed highlighter tool can be  to faciliate model creation and BehAPI traceability between process models and textual descriptions of law and regulations. rather than describing the process and  inspecting its alignment with law, our BehAPI forms part of the family of compliance-by-design (cbd) process development methodologies. the methodology involves the generation of formal models from the legal text, and the refinement of such models into the activities that conform a business process. the advantage of such BehAPI is that there is no further need for testing whether  execution of the process will violate a legal requirement.

testing
testing
testing
testing
testing
monitoring
monitoring
monitoring
monitoring
monitoring
types
types
types
types
types
bootcamp
bootcamp
bootcamp
bootcamp
bootcamp
bootcamp
bootcamp
bootcamp
bootcamp
bootcamp
bootcamp
bootcamp
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
BehAPI
